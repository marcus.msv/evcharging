package com.everon.assessment.ws.resources;

import com.everon.assessment.ws.domain.Session;
import com.everon.assessment.ws.dto.SessionDTO;
import com.everon.assessment.ws.enums.StatusEnum;
import com.everon.assessment.ws.services.SessionService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.testng.annotations.AfterTest;
import org.mockito.BDDMockito;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.Date;
import java.util.Optional;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.everon.assessment.ws.repository.SessionRepositoryTest.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class SessionResourceTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private SessionService sessionService;

    private static final String URL_BASE = "/api/session/";
    private  String id;
    private static final String statusIn = StatusEnum.IN_PROGRESS.name();
    private static final String statusFin = StatusEnum.FINISHED.name();
    private static final String stationID = "ABCD-12345";
    private static final Date DATA = new Date();

    @Test
    public void testCreateSession() throws Exception {

    }

    @Test
    public void testCreateNewSession() throws Exception {

        Session session = createNewSession();
        BDDMockito.given(this.sessionService.create(Mockito.any(Session.class))).willReturn(session);

        mvc.perform(MockMvcRequestBuilders.post(URL_BASE)
                .content(this.obterJsonRequisicaoPost())
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.data.stationId").value(stationID))
                .andExpect(jsonPath("$.errors").isEmpty());
    }

    @Test
    public void testUpdateSession() throws Exception {

        Session session = updateSession();
        BDDMockito.given(this.sessionService.create(Mockito.any(Session.class))).willReturn(session);

        mvc.perform(MockMvcRequestBuilders.post(URL_BASE)
                .content(this.obterJsonRequisicaoPost())
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.data.id").value(id))
                .andExpect(jsonPath("$.errors").isEmpty());
    }

    private String obterJsonRequisicaoPost() throws JsonProcessingException {
        SessionDTO sessionDTO = new SessionDTO();
        sessionDTO.setStationId(stationID);
        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(sessionDTO);
    }

    private Session createNewSession() {
        Session session = new Session();
        session.setStationId("ABCD-12345");
        return session;
    }

    private Session updateSession() {
        Session session = new Session();
        session.setId(id);
        return session;
    }

}
