package com.everon.assessment.ws.repository;

import com.everon.assessment.ws.domain.Session;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class SessionRepositoryTest {

    @Autowired
    private SessionRepository sessionRepository;

    private Session createNewSession(Session session) {
        Session session1 = new Session();
        session1.setStationId("ABCD-12345");
        return session;
    }

}
