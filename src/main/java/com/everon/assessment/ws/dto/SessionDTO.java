package com.everon.assessment.ws.dto;

import com.everon.assessment.ws.domain.Session;
import com.everon.assessment.ws.enums.StatusEnum;

import java.io.Serializable;
import java.util.Date;

public class SessionDTO implements Serializable {

    private static final long serialVersionUID = 1L;
    private String id;
    private String stationId;
    private Date startedAt;
    private Date stoppedAt;
    private StatusEnum status;
//    private Long totalCount;
//    private Long startedCount;
//    private Long stoppedCount;


    public SessionDTO() {

    }

    public SessionDTO(Session session){
        this.id =session.getId();
        this.stationId = session.getStationId();
        this.startedAt = session.getStartedAt();
        this.stoppedAt = session.getStoppedAt();
        this.status = session.getStatus();
//        this.totalCount = session.getTotalCount();
//        this.startedCount = session.getStartedCount();
//        this.stoppedCount = session.getStoppedCount();
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStationId() {
        return stationId;
    }

    public void setStationId(String stationId) {
        this.stationId = stationId;
    }

    public Date getStartedAt() {
        return startedAt;
    }

    public void setStartedAt(Date startedAt) {
        this.startedAt = startedAt;
    }

    public Date getStoppedAt() {
        return stoppedAt;
    }

    public void setStoppedAt(Date stoppedAt) {
        this.stoppedAt = stoppedAt;
    }

    public StatusEnum getStatus() {
        return status;
    }

    public void setStatus(StatusEnum status) {
        this.status = status;
    }

//    public Long getTotalCount() {
//        return totalCount;
//    }
//
//    public Long setTotalCount(Long totalCount) {
//        this.totalCount = totalCount;
//
//        return totalCount;
//    }
//
//    public Long getStartedCount() {
//        return startedCount;
//    }
//
//    public Long setStartedCount(Long startedCount) {
//        this.startedCount = startedCount;
//        return startedCount;
//    }
//
//    public Long getStoppedCount() {
//        return stoppedCount;
//    }
//
//    public Long setStoppedCount(Long stoppedCount) {
//        this.stoppedCount = stoppedCount;
//        return stoppedCount;
//    }

}

