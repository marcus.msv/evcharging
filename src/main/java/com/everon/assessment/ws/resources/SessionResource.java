package com.everon.assessment.ws.resources;

import com.everon.assessment.ws.dto.SessionDTO;
import com.everon.assessment.ws.services.SessionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.everon.assessment.ws.domain.Session;
import com.everon.assessment.ws.enums.StatusEnum;
import org.springframework.web.context.annotation.RequestScope;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api")
public class SessionResource {

    @Autowired
    SessionService sessionService;


    /**
     * lists all sessions available in memory.
     * @return ResponseEntity<Response<SessionDto>>
     */

    @RequestScope
    @GetMapping("/chargingSessions")
    public ResponseEntity<List<SessionDTO>> findAll() {
        List<Session> session = sessionService.findAll();
        List<SessionDTO> listDTO  = session.stream().map(x -> new SessionDTO(x)).collect(Collectors.toList());
        return ResponseEntity.ok().body(listDTO);
    }


    /**
     * Retrieve a summary of submitted charging sessions for the last minute
     * @return ResponseEntity<Response<SessionDto>>
     */

    @RequestScope
    @GetMapping("/chargingSessions/summary")
    public ResponseEntity<HashMap> findByGroup() {

        List<Session> session = sessionService.findAll();
        List<SessionDTO> sessionDTOS  = session.stream().map(x -> new SessionDTO(x)).collect(Collectors.toList());
        HashMap sessionSummary = summary(sessionDTOS);

    return ResponseEntity.ok().body(sessionSummary);
    }




    /**
     * Creates a new recharge session based on the charging station.
     *
     * @param sessionDTO
     * @return ResponseEntity<Response<SessionDTO>> | id
     */
    @RequestScope
    @PostMapping("/chargingSessions")
    public ResponseEntity<SessionDTO> create(@RequestBody SessionDTO sessionDTO) {
        Session session = sessionService.fromDTO(sessionDTO);
        session.setStationId(session.getStationId());
        session.setStartedAt(session.getStartedAt());
        session.setStoppedAt(null);
        session.setStatus(StatusEnum.IN_PROGRESS);
        return ResponseEntity.ok().body(new SessionDTO(sessionService.create(session)));
    }

    /**
     * Updates the charging status based on the id parameter generated when creating the session associated with a charging station.
     *
     * @param id
     * @return ResponseEntity<Response<Session>>
     */
    @RequestScope
    @PutMapping("/chargingSessions/{id}")
    public ResponseEntity<SessionDTO> update(@PathVariable String id, @RequestBody SessionDTO sessionDTO) {

        Session session = sessionService.fromDTO(sessionDTO);
        session.setId(id);
        session.setStoppedAt(session.getStoppedAt());
        session.setStatus(StatusEnum.FINISHED);
        return ResponseEntity.ok().body(new SessionDTO(sessionService.update(session)));
    }



    private HashMap<String, Integer> summary (List<SessionDTO> sessionDTOS) {

        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.MINUTE, -1);
        Date date = cal.getTime();

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd:hh:mm:ss");


        int totalCount  =  (int) sessionDTOS.stream().count();
        int startedCount = (int) sessionDTOS.stream().filter(c -> c.getStatus() == StatusEnum.IN_PROGRESS).count();
        int stoppedCount = (int) sessionDTOS.stream().filter(c -> c.getStatus() == StatusEnum.FINISHED).count();

        HashMap<String, Integer> sessionSummary = new HashMap<String, Integer>();

        sessionSummary.put( "totalCount", totalCount);
        sessionSummary.put("stoppedCount", stoppedCount);
        sessionSummary.put("startedCount",startedCount);


        return sessionSummary;

    }
}
