package com.everon.assessment.ws.domain;

import com.everon.assessment.ws.dto.SessionDTO;
import com.everon.assessment.ws.enums.StatusEnum;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;


@Document("session")
public class Session implements Serializable{

    private static final long serialVersionUID = 1L;
    @Id
    private String id;
    private String stationId;
    private Date startedAt;
    private Date stoppedAt;
    private StatusEnum status;


    public Session() {

    }

//    public Session(String id, String stationId, Date startedAt ,StatusEnum status) {
//        this.id = id;
//        this.stationId = stationId;
//        this.startedAt = startedAt;
//        this.status = status;
//    }


    public Session(String id ,String stationId, Date startedAt,Date stoppedAt ,StatusEnum status) {
        this.id = id;
        this.stationId = stationId;
        this.startedAt = startedAt;
        this.stoppedAt = stoppedAt;
        this.status = status;
    }

    public Session(SessionDTO sessionDTO){
        this.id = sessionDTO.getId();
        this.stationId = sessionDTO.getStationId();
        this.startedAt = new Date();
        this.stoppedAt = new Date();
        this.status = sessionDTO.getStatus();
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStationId() {
        return stationId;
    }

    public void setStationId(String stationId) {
        this.stationId = stationId;
    }

    public Date getStartedAt() {
        return startedAt;
    }

    public void setStartedAt(Date startedAt) {
        this.startedAt = startedAt;
    }

    public Date getStoppedAt() {
        return stoppedAt;
    }

    public void setStoppedAt(Date stoppedAt) {
        this.stoppedAt = stoppedAt;
    }

    public StatusEnum getStatus() {
        return status;
    }

    public void setStatus(StatusEnum status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Session)) return false;
        Session session = (Session) o;
        return Objects.equals(id, session.id);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id);
    }



    @Override
    public String toString() {
        return "Session [id=" + id + ", stationId=" + stationId + "]";
    }
}
