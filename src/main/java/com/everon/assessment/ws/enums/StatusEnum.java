package com.everon.assessment.ws.enums;

public enum  StatusEnum {
    IN_PROGRESS,
    FINISHED
}
