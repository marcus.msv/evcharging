package com.everon.assessment.ws.config;

import com.everon.assessment.ws.domain.Session;
import com.everon.assessment.ws.dto.SessionDTO;
import com.everon.assessment.ws.enums.StatusEnum;
import com.everon.assessment.ws.repository.SessionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.ContextRefreshedEvent;

import java.util.Date;
import java.util.Optional;

@Configuration
public class SetupDataLoader implements ApplicationListener<ContextRefreshedEvent> {

    @Autowired
    SessionRepository sessionRepository;

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {

//        final Date current = new Date();
//        Session bla = new Session("c2fc44f7-1989-4886-ad36-836599e57bb8","ABCD-12345", current, StatusEnum.IN_PROGRESS);
//        Session blas = new Session("c2fc44f7-1989-4886-ad36-836599e57bb8","ABCD-12345", current, StatusEnum.IN_PROGRESS);
//
//        createSessionIfNotExists(bla);
//        createSessionIfNotExists(blas);
    }

    private Session createSessionIfNotExists(final Session session){

        Optional<Session> obj = sessionRepository.findById(session.getId());

        if(obj.isPresent()) {
            return obj.get();
        }
        return sessionRepository.save(session);

    }
}
