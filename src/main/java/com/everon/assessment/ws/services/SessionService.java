package com.everon.assessment.ws.services;

import com.everon.assessment.ws.domain.Session;
import com.everon.assessment.ws.dto.SessionDTO;
import com.everon.assessment.ws.repository.SessionRepository;
import com.everon.assessment.ws.services.exception.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class SessionService {

    @Autowired
    private SessionRepository sessionRepository;

    @Transactional
    public List<Session> findAll() {
        return sessionRepository.findAll();
    }

    @Transactional
    public List<Session> findAByGroup() {
        return sessionRepository.findAll();
    }

    @Transactional
    public Session findById(String id) {
        Optional<Session> session = sessionRepository.findById(id);
        return session.orElseThrow(() -> new ObjectNotFoundException("Object not found"));
    }

    @Transactional
    public Session create(Session session){
        return sessionRepository.save(session);
    }

    @Transactional
    public Session fromDTO (SessionDTO sesionDTO){
        return new Session(sesionDTO);
    }

    @Transactional
    public Session update(Session session) {
        Optional<Session> updateSession = sessionRepository.findById(session.getId());
        return updateSession.map(u -> sessionRepository.save(new Session(u.getId(), u.getStationId(), u.getStartedAt() ,session.getStoppedAt(), session.getStatus())))
                .orElseThrow(() -> new ObjectNotFoundException("SessionId not found"));
    }
}
