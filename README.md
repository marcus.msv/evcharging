# Assessment Everon
API for Data Engineer Assessment at Everon

This API aims to implement 4 endpoints with the following characteristics:
```
@GetMapping ("/chargingSessions")
Return all sessions available on the system in progress and finished

@GetMapping ("/chargingSessions/summary")
Returns sessions grouped by status and an attribute with total sessions

@PostMapping ("/chargingSessions")
Creates a new session based on a charging station

@PutMapping ("/chargingSessions/{id}")
Updates the end of a recharge when calling based on the session ID acquired when registering a recharge
```
### Details of API RESTful
The API RESTful of Everon contains the characteristics below:  
* Spring Boot e Java 8
* Database MongoDB

### How to Execute the Application
Maven is mandatory and add to PATH of your operational system
```
download the application.file file sent by email 
git clone https://gitlab.com/marcus.msv/evcharging.git
cd evcharging
copy the application.file file to the directory: src/main/resources/
mvn spring-boot:run
Access the endpoints through the url http://localhost:8080
```
### Document of Endpoints is available in Swagger in the link below
http://localhost:8080/swagger-ui.html/

